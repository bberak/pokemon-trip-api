var express = require('express');
var pokegoScan = require('pokego-scanner');
var poketrip = require('./poketrip');
var cache = require('apicache').options({ debug: true }).middleware;

var app = express();
var port = process.env.PORT || 3000;
var pt = new poketrip();

app.get('/scan/:latitude/:longitude/:distance', cache('30 seconds'), function (req, res) {  
	var coords = {
		latitude: req.params.latitude,
	    longitude: req.params.longitude
	};
	pokegoScan(coords, {distance: req.params.distance}, function(err, pokemon) {
		if (err) throw err;
	    res.send(pokemon);
	});
});

app.get('/stops/:latitude/:longitude/:distance', function (req, res) {
	res.send([
		{stop_lat: -33.8858330 + 0.001, stop_lon: 151.2075480 + 0.001, _id: 1, stop_name: "Central Station"},
		{stop_lat: -33.8858330 + 0.001, stop_lon: 151.2075480 + 0.001, _id: 2, stop_name: "Central Station"},
		{stop_lat: -33.8858330 + 0.001, stop_lon: 151.2075480 + 0.001, _id: 3, stop_name: "Central Station"},
		{stop_lat: -33.8858330 + 0.001, stop_lon: 151.2075480 + 0.001, _id: 4, stop_name: "Central Station"}])
});

app.get('/stopsnear/:latitude/:longitude/:distance', function (req, res) {
	pt.getTransitStops(req.params.latitude, req.params.longitude, req.params.distance, function(e, result) {
		if (e) {
			console.log("Stopsnear: ");
			console.log(e)
			throw e;
		}
		res.send(result);
	});
});

app.get('/tripsfrom/:stopid/:minutes', function (req, res) {
	var currentDate = new Date();
	var endDate = new Date(currentDate.getTime() + req.params.minutes*60000);
	var current_hour = (currentDate.getHours() < 10 ? '0':'') + currentDate.getHours();
	var current_minutes = (currentDate.getMinutes()  < 10 ? '0':'') + currentDate.getMinutes();
	var end_hour = (endDate.getHours() < 10 ? '0':'') + endDate.getHours();
	var end_minutes = (endDate.getMinutes()  < 10 ? '0':'') + endDate.getMinutes();
	var currentTime = current_hour + ':' + current_minutes;
	var endTime = end_hour + ':' + end_minutes;

	poketrip.prototype.getTripsByStop(req.params.stopid, currentTime, endTime, function (e, result) {
		res.send(result);
	});
});

app.get('/trips/:stop_id', function (req, res) {
	res.send([
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001, id: 1, name: "10:09 AM, Platform 28"},
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001, id: 2, name: "10:19 AM, Platform 23"},
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001, id: 3, name: "10:22 AM, Platform 07"},
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001, id: 4, name: "10:09 AM, Platform 18"},
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001, id: 5, name: "10:09 AM, Platform 2"}])
});



app.listen(port, function () {
  console.log('API listening on port ' + port );
});
