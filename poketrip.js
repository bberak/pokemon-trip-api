var async = require("async");
var gtfs = require('./node-gtfs');

const AGENCY_KEY = 'TfNSW'
const CURRENT_TIME = null;
const DEFAULT_LAT = -33.886964;
const DEFAULT_LON = 151.208301;
const DEFAULT_SEARCH_RADIUS = 0.15; // Radius in miles


var poketrip = function () {};

poketrip.prototype.getTransitStops = function (lat, lon, radius, cb) {
  console.log('Getting transit stops at lat: ' + lat + ' lon: ' + lon + ' radius: ' + radius);
  return gtfs.getStopsByDistance(lat || DEFAULT_LAT, lon || DEFAULT_LON, radius || DEFAULT_SEARCH_RADIUS, function(err, stops) {
    cb(false, stops);
    return stops;
  });
}

poketrip.prototype.getTripsByStop = function (stopId, startTime, endTime, cb) {
  if(!stopId) cb(true, 'A stop_id must be specified');
  if(!startTime) cb(true, 'A start_time must be specified');
  if(!endTime) cb(true, 'A end_time must be specified');
  gtfs.getStoptimesByStopRestrictedByDepartureTime(AGENCY_KEY, stopId, startTime, endTime, null, function(err, trips) {
      var stoptimes = [];
      async.parallel(trips.map(function(trip) {
        return function(callback) {
          gtfs.getStoptimesOnTripFromStopSequenceUntilTime(AGENCY_KEY, trip.trip_id, trip.stop_sequence, endTime, function(err, stops) {
              stoptimes.push({
                stops: stops,
                trip_id: trip.trip_id
              });
              callback(null, {
                stops: stops
              })
          });
        };
      }), function (a) {
        cb(false, stoptimes);
      });
  });
};

// poketrip.prototype.getTripsNearMe = function (lat, lon, radius) {
//
//   return gtfs.getStopsByDistance(DEFAULT_LAT, DEFAULT_LON, SEARCH_RADIUS, function(err, stops) {
//     console.log('Found ' + stops.length + ' stops within a ' + SEARCH_RADIUS + ' mile radius of your current location');
//     console.log(stops);
//
//     rl.question('Please choose a stop: ', function (response) {
//
//         var stopId = stops[response - 1].stop_id;
//
//         gtfs.getStoptimesByStopRestrictedByDepartureTime(AGENCY_KEY, stopId, '10:00:00', '10:30:00', null, function(err, trips) {

                // var selectedTrip = stoptimes[response];
                // var startShapeDistance = selectedTrip.shape_dist_traveled;
                // var startStopSequence = selectedTrip.stop_sequence;
                // var tripId = selectedTrip.trip_id;
                // console.log(trips.length + ' possible PokeTrips from this stop:');
                // trips.forEach(function(trip) {
                //
                //   console.log('Trip ' + trip.trip_id);
                //
                //
                //                 gtfs.getStoptimesOnTripFromStopSequenceUntilTime(AGENCY_KEY, trip.trip_id, trip.stop_sequence, '10:30:00', function(err, stops) {
                //
                //                   console.log('Stops on Trip ' + stops[0].trip_id + ':');
                //                   console.log(stops);


                                  //console.log('Starting at: ' + startShapeDistance + ', ending at: ' + endShapeDistance);
                                  // gtfs.getShapesByTrip(AGENCY_KEY, selectedTrip.trip_id, function (err, shapes) {
                                  //   console.log(shapes);
                                  //   shapes.forEach(function(shape) {
                                  //     shape.sort(function(a, b) {
                                  //       return a.shape_pt_sequence - b.shape_pt_sequence;
                                  //     });
                                  //     var line = shape.map(function(shape_pt) {
                                  //       return shape_pt.loc;
                                  //     });
                                  //     coordinates.push(line);
                                  //   });
                                  //
                                  // })
//                                 });
//
//
//
//
//                 });
//
//
//         });
//
//       rl.close();
//     });
//   });
// }

module.exports = poketrip;



// Get shapes or coords by routes
// gtfs.getShapesByRoute(AGENCY_KEY, '31-393-sj2-3', function (err, shapes) {
//   console.log('Found ' + shapes.length + ' shapes');
//
//   console.log(shapes.map(function (x) {
//     return x.map(function (y) {
//       return y.loc;
//     });
//
//   }))
//
// })

// gtfs.getCoordinatesByRoute(AGENCY_KEY, '31-393-sj2-3', function (err, coords) {
//   console.log('Found coords');
//   console.log(coords);
//
// })

// Find stops near you




  // // Routes running from this stop
  // gtfs.getRoutesByStop(AGENCY_KEY, stopId, function(err, routes) {
  //   console.log(routes.length + ' routes running from stop ' + stopId);
  //   console.log(routes.map(function (x, i) {
  //     var obj = {};
  //     obj[i + 1] = x.route_id;
  //     return obj;
  //
  //   }))
  //
  //   rl.question('Please choose a route: ', function (response) {
  //       // TODO: Log the answer in a database
  //
  //
  //       var selectedRoute = routes[response - 1];
  //       console.log('Getting stop times for route ' + selectedRoute.route_id  +  ' at stop ' + stopId + '...');
  //
  //       gtfs.getStoptimesByStopRestrictedByDepartureTime(AGENCY_KEY, stopId, '10:00:00', '10:30:00', null, function(err, stoptimes) {
  //         console.log(stoptimes);
  //       });
  //       rl.close();
  //   });
  //
  //   //var routeId = routes[routes.length - 1].route_id;
  //
  //
  // });



// Find trips
// gtfs.getTripsByRouteAndDirection(AGENCY_KEY, '31-395-sj2-3', 0, null, function(err, trips) {
//   console.log('Eshays Adlays');
//   console.log(trips);
//   var tripId = trips[trips.length - 1].trip_id;
//   // Get stop times for last trip
//   console.log('Stop times');
//   gtfs.getStoptimesByTrip(AGENCY_KEY, tripId, function(err, stoptimes) {
//     console.log('STOP TIMES');
//     console.log(stoptimes);
//   });
//
//
// });


// console.log('Finding routes near you...');
//
// gtfs.getRoutesByDistance(LAT, LON, SEARCH_RADIUS, function(err, routes) {
//   console.log('Found ' + routes.length + ' routes');
//   console.log(routes);
// });

// gtfs.findBothDirectionNames(AGENCY_KEY, '31-395-sj2-3', function(err, directionNames) {
//   console.log('Found both direction names');
//   console.log(directionNames);
// });
//



//
// console.log('Route shape');
//
// gtfs.getCoordinatesByRoute(AGENCY_KEY, '31-M50-sj2-3', function(err, coordinates) {
//     console.log('Got route coordinates');
//     console.log(coordinates);
//
// });



// gtfs.agencies(function(err, agencies) {
//   console.log('found agencies');
//   console.log(agencies);
// });
